<!DOCTYPE html>
<html>
<head>
	<title>DB Creation -MySqli Procedural</title>
</head>
<body>
	<?php
		$servername = "localhost";
		$username = "root";
		$password = "";

		//create connection 
		$conn = mysqli_connect($servername,$username,$password);
		//check connection
		if(!$conn){
			die("Connection failed : " . mysqli_connect_error());
		}
		//create database
		$sql = "CREATE DATABASE myDBProceduralSaya";
		if(mysqli_query($conn,$sql)){
			echo "Database created successfull";
		}
		else {
			echo "Error Creating database : " . mysqli_error($conn);
		}

		mysqli_close($conn);
	?>
</body>
</html>