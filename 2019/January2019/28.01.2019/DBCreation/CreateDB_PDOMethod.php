<!DOCTYPE html>
<html>
<head>
	<title>Create DB PDO</title>
</head>
<body>
	<?php
		$servername = "localhost";
		$username = "root";
		$password = "";
		try{
				$conn = new PDO("mysql:host=$servername",$username,$password);
				//set the PDO error mode to connection 
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql = "CREATE DATABASE myDBPDOSaya";
				//use exec() because no results are returned
				$conn->exec($sql);
				echo "Database created successfully <br>";
		}
		catch(PDOException $e){
			echo $sql . "<br>" .$e->getMessage();
		}
		$conn = null;
	?>
</body>
</html>