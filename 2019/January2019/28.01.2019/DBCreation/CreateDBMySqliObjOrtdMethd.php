<!DOCTYPE html>
<html>
<head>
	<title>Create DB -Mysqli Obj Oriented Approach</title>
</head>
<body>
	<?php
		$servername = "localhost";
		$username = "root";
		$password = "";
		//create connection 
		$conn = new mysqli($servername,$username,$password);
		//check connection 
		if ($conn->connect_error){
			die("connection failed : " . $conn->connect_error);
		}
		//create database
		$sql = "CREATE DATABASE mysqliobjSaya";
		if($conn->query($sql) === TRUE){
			echo "Database created successfully";
		}
		else {
			# code...
			echo "Error creating database:" .$conn->error;
		}
		$conn->close();

	?>
</body>
</html>