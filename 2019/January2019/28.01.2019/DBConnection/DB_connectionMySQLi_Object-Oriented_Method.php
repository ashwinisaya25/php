<!DOCTYPE html>
<html>
<head>
	<title>Database Connection</title>
</head>
<body>
	<?php
		echo "<h2>Open a Connection to MySQL.<br><i>MySQLi Object-Oriented Method</i></h2>";
		$servername = "localhost";
		$username = "root";
		$password = "";
		//create connection
		$conn = new mysqli($servername,$username,$password);
		//check connection
		if ($conn->connect_error) {
			# code...
			die("Connection failed: ". $conn->connect_error);
		}
		echo "Connected Successfully";
		$conn->close();
	?>
</body>
</html>