<!DOCTYPE html>
<html>
<head>
	<title>MySQLi Procedural</title>
</head>
<body>
	<?php
		$servername = "localhost";
		$username = "root";
		$password = "";
		//create connection 
		$conn = mysqli_connect($servername,$username,$password);
		//check connection 
		if (!$conn) {
			# code...
			die("connection failed :" . mysqli_connect_error());
		}
		echo "Connected successfully";
		mysqli_close($conn);

	?>
</body>
</html>