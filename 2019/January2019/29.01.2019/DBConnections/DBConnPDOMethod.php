<!DOCTYPE html>
<html>
<head>
	<title>PDO Example</title>
</head>
<body>
	<?php
		$servername = "localhost";
		$username = "root";
		$password = "";
		try{
			$conn = new PDO("mysql:host=$servername;dbname=sample;",$username,$password);
			//set the PDO error mode to exception 
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			echo "Connected successfuly";
		}
		catch(PDOException $e){
			echo "Connection failed:" . $e->getMessage();
		}
		$conn = null;
	?>
</body>
</html>