<!DOCTYPE html>
<html>
<head>
	<title>Insert Values </title>
</head>
<body>
	<?php
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "mysqliobjsaya";
		//create connection 
		$conn = new mysqli($servername, $username, $password, $dbname);
		//check connection 
		if($conn->connect_error){
			die("connection failed : " . $conn->connect_error);
		}
		$sql = "INSERT INTO myguests(firstname, lastname, email) VALUES ('Sita', 'Ram','sita@ram.com')";
		//echo "INSERT INTO myguests(firstname, lastname, email) VALUES ('Sita', 'Ram','sita@ram.com')";
		
		if($conn->query($sql) === TRUE){
			echo "New record created successfully";
		}
		else{
			echo "Error: " . $sql . "<br>" . $conn->error;
		}
		$conn->close();
	?>
</body>
</html>