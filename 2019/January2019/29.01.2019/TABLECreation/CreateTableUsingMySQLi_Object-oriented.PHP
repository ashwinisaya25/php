<!DOCTYPE html>
<html>
<head>
	<title>Create Table -MySqli Object Oriented Method</title>
</head>
<body>
	<?php
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "mysqliobjsaya";
		//create connection 
		$conn = new mysqli($servername,$username,$password,$dbname);
		//check connection 
		if($conn->connect_error){
			die("Connection failed" . $conn->connect_error);
		}
		//create database SqliObj
		//sql to create table 
		$sql = "CREATE TABLE MyGuests(
			id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			firstname VARCHAR(30) NOT NULL,
			lastname VARCHAR(30) NOT NULL,
			email VARCHAR(30) ,
			reg_date TIMESTAMP
		)";
		
		if($conn->query($sql) === TRUE){
			echo "Table MyGuests Created Successfully";
	       }
	    else{
	    	echo "Error creating table: " . $conn->error;
	    }
	    $conn->close();
	?>
	
		
</body>
</html>