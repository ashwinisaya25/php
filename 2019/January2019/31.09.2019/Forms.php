<!DOCTYPE html>
<html>
<head>
	<title>Form Validation</title>
	<script type="text/javascript">
		function validateForm(){
			//var x = document.forms["myForm"][name].value;
			if(document.myForm.name.value=="") {
			
				alert(" Enter Your Name  ");
				return false;
			}
			else if(document.myForm.email.value=="") {
				alert(" Enter Your Email");
				return false;
			}
			else if(document.myForm.website.value=="") {
				alert(" Enter Your Website URL");
				return false;
			}
			else{
				return true;
			}
		}
		
	</script>
</head>
<body>
	<?php
		//define variables and set to empty values
		$name = $email = $gender = $comment = $website = "";
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
		
			$name = test_input($_POST["name"]);
			$email = test_input($_POST["email"]);
			$website = test_input($_POST["website"]);
			$comment = test_input($_POST["comment"]);
			$gender = test_input($_POST["gender"]);
		}
		function test_input($data) {
			$data = trim($data);
			$data = stripslashes($data);
			$data = htmlspecialchars($data);
			return $data;
		}
	?>
	<h2>PHP Form Validation Example</h2>
	<form name="myForm" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" 
	onsubmit="return validateForm()">
	
		Name : <input type="text" name="name"><br></br>
		Email : <input type="text" name="email"><br></br>
		Website : <input type="text" name="website"><br></br>
		Comment : <textarea name="comment" rows="5" cols="40"></textarea> <br></br>

		Gender :
		<input type="radio" name="female" value="Female">Female
		<input type="radio" name="male" value="Male">Male
		<input type="radio" name="other" value="Other">Other<br></br>
		<input type="submit" name="submit" value="Submit">

	</form>
	<?php
		echo "<h2>Your input :</h2>";
		echo $name;
		echo "<br>";
		echo $email;
		echo "<br>";
		echo $website;
		echo "<br>";
		echo $comment;
		echo "<br>";
		echo $gender;
	?>
</body>
</html>