<!DOCTYPE html>
<html>
<head>
	<title>Update records</title>
</head>
<body>
	<?php
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "SAIASHWINI";

		//create connection
		$conn = new mysqli($servername, $username, $password, $dbname);

		//check connection
		if($conn->connect_error){
			die("COnnection failed: " . $conn->connect_error);
		}

		$sql = "UPDATE MyGUests SET lastname = 'Rama' WHERE id = 2";

		if($conn->query($sql) === TRUE){
			echo "Record updated successfully";
		}
		else{
			echo "Error updating record: " . $conn->error;
		}
		$conn->close();
	?>
</body>
</html>