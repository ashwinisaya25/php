<!DOCTYPE html>
<html>
<head>
	<title>Update Records</title>
</head>
<body>
	<?php
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "SAIASHWINI";

		//create connection
		$conn = mysqli_connect($servername, $username, $password, $dbname);
		//check connection
		if(!$conn){
			die("Connection failed: " . mysqli_connect_error());
		}
		$sql = "UPDATE MyGuests SET lastname = 'Ram' WHERE id = 2";
		if(mysqli_query($conn, $sql)){
			echo "Record updated successfully";
		}
		else{
			echo "Error updating record: " . mysqli_error($conn);
		}
		mysqli_close($conn);
	?>
</body>

</html>