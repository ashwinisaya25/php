<!DOCTYPE html>
<html>
<head>
	<title>MySqli PDO Delete Statement</title>
</head>
<body>
	<?php
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "";
		try{
			$conn = new PDO("mysql:host=$servername;dbname=",$username,$password);
			//set the err mode to exception
			$conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

			//sql to delete a record 
			$sql = "DELETE FROM MyGuest WHERE id=3";

			// ude exec() because no results are returned 
			$conn->exec($sql);
			echo "Record deleted successfully";
		}
		catch(PDOException $e){
			echo $sql . "<br>" . $e->getMessage();
		} 
		$conn = null;
	?>
</body>
</html>