<!DOCTYPE html>
<html>
<head>
	<title>PHP math and assignment operators</title>
</head>
<body>
	<?php
		$x = 10;
		$y = 6;
		echo $x + $y;
		echo $x - $y;
		echo $x * $y;
		echo $x / $y;
		echo $x % $y;
		echo $x ** $y;
		$z = 9;
		echo $z;
		$a = 20;
		$a += 100;
		echo $a;
		$a = 50;
		$a -= 30
		echo $a;
		$a = 50;
		$a -= 30;
		echo $a;
		$s = 10;
		$a = 6;
		echo $s * $a;
		$s = 10;
		$s /= 5;
		echo $s;
		$s = 15;
		$s %= 4;
		echo $s;
		$s = 100;
		$a = "9";
		var_dump($s == $a);
		$s = 100;
		$a = "100";
		var_dumps($s === $a);//returns false because types are not equal;
		$s = 100;
		$a = "100";
		var dumps($s != $a);//returns false because values are equal
		$s = 100;
		$a = "100";
		var_dump($s <> $a);//returns false beacause values are equal
		$s = 9;
		$a = "9";
		var_dump($s !== $a);//returns true because types are not equal
		$s = 9;
		$a = 5;
		var_dumps($s > $a);//returns true because $s is greater than $a
		$s = 9;
		$a = 5;
		var_dumps($s < $a);//returns true because $s is less than $a
		$s = 9;
		$a = 9;
		var_dumps($s >= $a);//returns true because $s is greater than or equal to $a
	?>
</body>
</html>