<!DOCTYPE html>
<html>
<head>
	<title>PHP math and assignment operators</title>
</head>
<body>
	<?php
		$x = 10;
		$y = 6;
		echo $x + $y;
		echo "<br>";
		echo $x - $y;
		echo "<br>";
		echo $x * $y;
		echo "<br>";
		echo $x / $y;
		echo "<br>";
		echo $x % $y;
		echo "<br>";
		echo $x ** $y;
		echo "<br>";
		$z = 9;
		echo $z;
		echo "<br>";
		$a = 20;
		$a += 100;
		echo $a;
		echo "<br>";
		/*$s = 50;
		$a -= 30
		echo $a;*/
		$a = 50;
		$a -= 30;
		echo $a;
		echo "<br>";
		$s = 10;
		$a = 6;
		echo $s * $a;
		echo "<br>";
		$s = 10;
		$s /= 5;
		echo $s;
		echo "<br>";
		$s = 15;
		$s %= 4;
		echo $s;
		echo "<br>";
		$s = 100;
		$a = "9";
		var_dump($s == $a);
		echo "<br>";
		/*$s = 100;
		$a = "100";
		var_dumps($s === $a);*///returns false because types are not equal;
		/*$s = 100;
		$a = "100";
		var dumps($s != $a);*///returns false because values are equal
		$s = 100;
		$a = "100";
		var_dump($s <> $a);//returns false beacause values are equal
		echo "<br>";
		$s = 9;
		$a = "9";
		var_dump($s !== $a);//returns true because types are not equal
		echo "<br>";
		/*$s = 9;
		$a = 5;
		var_dumps($s > $a);//returns true because $s is greater than $a
		$s = 9;
		$a = 5;
		var_dumps($s < $a);//returns true because $s is less than $a
		$s = 9;
		$a = 9;
		var_dumps($s >= $a);*///returns true because $s is greater than or equal to $a
		$s = 10;
		$a = 5;
		echo ($x <=> $y);
		echo "<br>";
		$s = 10;
		echo ++$s;
		echo "<br>";
		$s = 10;
		echo $s++;
		echo "<br>";
		$s = 10;
		echo --$s;
		echo "<br>";
		$s = 10;
		echo $s--;
		echo "<br>";
		$s = 100;
		$a = 50;
		if($s == 100 && $a == 50){
			echo "Satyam";
			echo "<br>";
		}
		$s = 100;
		$a = 50;
		if($s == 100 || a == 80){
			echo "Satyam";
			echo "<br>";
		}
		$s = 100;
		if($s !== 90){
			echo "Satyam";
			echo "<br>";
		}
		/*$s = Badra;
		$a = Krishna;
		echo $s . $a;*/
		$s = "Rukmini";
		$a = "Krishnaa";
		$s .= $a;
		echo $s;
		echo "<br>";
		$s = array("a" => "red" , "b" => "green");
		$a = array("c" => "blue" , "d" => "yellow");
		print_r($x + $y);
		echo "<br>";


	?>
</body>
</html>