<!DOCTYPE html>
<html>
<head>
	<title>Global and Local Variables</title>
</head>
<body>
	<?php
		$x = 5;//global scope
		function myTest(){
			//using x inside this function will generate an error
			echo "<p>Variable x inside function is : $x</p>";
		}
		myTest();

		echo "<p>Variables x outside function is : $x</p>";
	?>

</body>
</html>