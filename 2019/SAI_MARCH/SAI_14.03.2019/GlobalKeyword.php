<!DOCTYPE html>
<html>
<head>
	<title>Global Keyword</title>
</head>
<body>
	<?php
		$x = 5;
		$y = 10;
		function myTest(){ //run function
		echo $y; // output the new value for variable $y
		}
		myTest(); // run function
		echo $y; // output the new value for variable $y
	?>
</body>
</html>