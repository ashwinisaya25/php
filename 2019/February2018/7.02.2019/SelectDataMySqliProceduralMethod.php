<!DOCTYPE html>
<html>
<head>
	<title>Select Data from the table using MySqli Procedural Method</title>
</head>
<body>
	<?php
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "";
		//create connection 
		$conn = mysqli_connect($servername,$username,$password,$dbname);
		if(!$conn){
			die("Connection failed: ".mysqli_connect_error());
		}
		$sql = "SELECT id, firstname,lastname FROM MyGuests";
		$result = mysqli_query($conn,$sql);

		if(mysli_num_rows($result)>0){
			//output data of each row 
			while($row = mysli_assoc($result)){
				echo "id : " . $row["id"]."-Name: ".$row["firstname"]." ".$row["lastname"]."<br>";
			}
		}
		else
		{
			echo "0 results";
		}
		mysqli_close($conn);
		
	?>


</body>
</html>