<!DOCTYPE html>
<html>
<head>
	<title>Select data</title>
</head>
<body>
	<?php
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "saisaya";

		//create connection 
		$conn = new mysqli($servername, $username,$password,$dbname);
		//check connenction 
		if ($conn->connect_error){
			die("Connenction failed: " .$conn->connect_error);
		}
		$sql = "SELECT id, firstname,lastname FROM myguests";
		$result = $conn->query($sql);
		if($result->num_rows>0){
			//output data of each row 
			while ($row = $result->fetch_assoc()) {
				# code...
				echo "id: ". $row["id"] . "- Name: " . $row["firstname"]. " " . $row["lastname"]."<br>";
			}
		}
		else {
			# code...
			echo "0 results";
		}
		$conn->close();
	?>

</body>
</html>